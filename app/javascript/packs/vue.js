/* eslint no-console: 0 */



import Vue from 'vue/dist/vue.esm'
import Vuetify from 'vuetify'
import App from '../app.vue'
import('../../../node_modules/vuetify/dist/vuetify.min.css')
Vue.use(Vuetify)
document.addEventListener('DOMContentLoaded', () => {
  const app = new Vue({
    el: '#vue',
    data: {
      message: "Can you say hello?"
    },
    components: { App }
  })
})

//
//
// If the using turbolinks, install 'vue-turbolinks':
//
// yarn add 'vue-turbolinks'
//
// Then uncomment the code block below:
//
// import TurbolinksAdapter from 'vue-turbolinks';
// import Vue from 'vue/dist/vue.esm'
// import App from '../app.vue'
//
// Vue.use(TurbolinksAdapter)
//
// document.addEventListener('turbolinks:load', () => {
//   const app = new Vue({
//     el: '#hello',
//     data: {
//       message: "Can you say hello?"
//     },
//     components: { App }
//   })
// })
